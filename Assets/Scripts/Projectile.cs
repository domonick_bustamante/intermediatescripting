using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{
	
	//public float;
	//public float;
	private string creator; // who created this projectile
	public float speed = 3.0f;
	public float lifetime = 5.0f;
	
	// Use this for initialization
	void Start () 
	{
		rigidbody2D.velocity = transform.right * speed;

		Destroy (this.gameObject , lifetime);
	}
	
	void OnTriggerEnter2D ( Collider2D other ) 
	{
		if (other.tag == "Player" && creator == "Enemy") 
		{
			// deal the damage
			Debug.Log ("Hit Player");
			GameController.Instance.AddPoints ( 1.0f );
			Destroy ( this.gameObject ); // destroys object upon impact with player

		} else if (other.tag == "Enemy" && creator == "Player") 
		{
			// deal the damage to the enemy
			GameController.Instance.AddPoints ( 1.0f );
			Debug.Log ("Hit Enemy");
			Destroy ( this.gameObject );

		}
	}
	
	public void CreatedBy ( string tag ) 
	{
		creator = tag;
	}
	

}
